FROM centos:latest
USER root

RUN yum install httpd -y

RUN echo "The Web Server is running" > /var/www/html/index.html
EXPOSE 80

CMD ["-D", "FOREGROUND"]
ENTRYPOINT ["/usr/sbin/httpd"]
